import numpy as np
import copy
import math
import matplotlib.pyplot as plt
import csv

def obtenerArchivo(archivo, numeroDatos):

    return funcionLeerArchivo( archivo, numeroDatos)

def funcionLeerArchivo(archivo, numeroDatos):
    valorIncorrecto = -1
    datosCrudos = np.genfromtxt(archivo ,delimiter=',')

    #Tamano de datos lectura n datos
    datosCrudos =  datosCrudos[len(datosCrudos) - numeroDatos:]

    return normalizaDatos(datosCrudos, valorIncorrecto)
    #return datosCrudos

def getDataFormatted(datos, neuronasCentrada, numeroPasosPronostico):

    inputArray = []
    targetArray = []

    temporal = (len(datos) - neuronasCentrada - numeroPasosPronostico + 1)

    for i in range (0, temporal):

        input = datos[ i: i + neuronasCentrada]
        target = datos[ i + neuronasCentrada + (numeroPasosPronostico - 1) ]

        if "NA" not in input and target != "NA":
            inputArray.append(input)
            targetArray.append(target)

    input = np.array(inputArray)
    inp = input.reshape(len(input), neuronasCentrada)

    target = np.array(targetArray)
    lenT = len(target)
    tar = target.reshape(lenT, 1)

    return inp, tar

def formatInputTarget(datos, neuronasCentrada):
    inputArray = []
    targetArray = []

    for i in range (0, ( len(datos) - neuronasCentrada - 1)):

        input = datos[ i : i + neuronasCentrada ]
        target = datos[ i + neuronasCentrada ]

        if "NA" not in input and target != "NA":
            inputArray.append(input)
            targetArray.append(target)

    input = np.array(inputArray)
    inp = input.reshape(len(input), neuronasCentrada)

    target = np.array(targetArray)
    lenT = len(target)
    tar = target.reshape(lenT, 1)

    return inp, tar

def formatear(datos):

    datosArray = []

    for i in range(0,len(datos)):

        if datos[i] != "NA":
            datosArray.append(datos[i])

    return datosArray

def normalizaDatos(datosCrudos, valorIncorrecto):

    datosCrudosSinValorIncorrecto = copy.copy(datosCrudos)

    datosCrudosSinValorIncorrecto = filter( lambda x: x != -1, datosCrudosSinValorIncorrecto )

    #Normalizacion de los datos
    numeroMaximo = np.amax( datosCrudosSinValorIncorrecto )

    # print "numeroMaximo: ", numeroMaximo
    numeroMinimo = np.amin( datosCrudosSinValorIncorrecto )

    # print "numeroMinimo: ", numeroMinimo
    return map(lambda x : "NA" if x == -1 else (( x - numeroMinimo ) / ( numeroMaximo - numeroMinimo)), datosCrudos)

def valoresMinMax(archivo, numeroDatos):

    valorIncorrecto = -1
    datosCrudos = np.genfromtxt(archivo ,delimiter=',')

    #Tamano de datos lectura n datos
    datosCrudos =  datosCrudos[len(datosCrudos) - numeroDatos:]

    datosCrudosSinValorIncorrecto = copy.copy(datosCrudos)

    datosCrudosSinValorIncorrecto = filter( lambda x: x != -1, datosCrudosSinValorIncorrecto )

    #Normalizacion de los datos
    numeroMaximo = np.amax( datosCrudosSinValorIncorrecto )
    # print "numeroMaximo: ", numeroMaximo
    numeroMinimo = np.amin( datosCrudosSinValorIncorrecto )
    # print "numeroMinimo: ", numeroMinimo
    return numeroMinimo, numeroMaximo

def promedioLista(lista):
    sum = 0.0

    for i in range(0,len(lista)):
        sum = sum + lista[i]

    return sum / len(lista)

def desnomalizar(target, output, numeroMinimo, numeroMaximo):

    outDes = np.array([])
    tarDes = np.array([])

    for x in range(0,len(target)):
        targetDes = ( target[x] * (numeroMaximo-numeroMinimo) ) + numeroMinimo
        tarDes = np.append(tarDes, targetDes )

    for y in range(0,len(output)):
        outputDes = ( output[y] * (numeroMaximo-numeroMinimo) ) + numeroMinimo
        outDes = np.append(outDes, outputDes )

    return tarDes, outDes

def imprimirResultado(tar, out, numeroMinimo, numeroMaximo, algoritmoEntrenamiento, nb_archivo, neuronasCentrada, neuronasCOculta, t):

    arrayMSE  = []
    arrayMAPE  = []
    arraySMAPE  = []
    arrayMSEDes  = []
    arrayMAPEDes  = []
    arraySMAPEDes  = []

    # se desnormaliza los datos
    tarDes, outDes = desnomalizar(tar, out, numeroMinimo, numeroMaximo)

    for i in range(0, len(tar) ):

        # Errores Normalizados
        mse = (tar[i] - out[i]) ** 2

        if tar[i] == 0:
            mape = 0
        else:
            mape = abs(tar[i] - out[i])/abs(tar[i])

        smape = abs(out[i] - tar[i]) / ( ( abs(tar[i]) + abs(out[i]) ) / 2 )

        arrayMSE.append(mse)
        arrayMAPE.append(mape)
        arraySMAPE.append(smape)

        # Errores Desnomalizados
        mse = (tarDes[i] - outDes[i]) ** 2
        if tarDes[i] == 0:
            mape = 0
        else:
            mape = abs(tarDes[i] - outDes[i])/abs(tarDes[i])

        smape = abs(outDes[i] - tarDes[i]) / ( ( abs(tarDes[i]) + abs(outDes[i]) ) / 2 )

        arrayMSEDes.append(mse)
        arrayMAPEDes.append(mape)
        arraySMAPEDes.append(smape)

    #Normalizados
    totalMSE = promedioLista(arrayMSE)
    totalMAPE = promedioLista(arrayMAPE)
    totalSMAPE = promedioLista(arraySMAPE)

    totalRMSE = math.sqrt(totalMSE)

    # Desnormalizados
    totalDesMSE = promedioLista(arrayMSEDes)
    totalDesMAPE = promedioLista(arrayMAPEDes)
    totalDesSMAPE = promedioLista(arraySMAPEDes)

    totalDesRMSE = math.sqrt(totalDesMSE)

    #Se asigna el algoritmo de entrenamiento
    if algoritmoEntrenamiento == 0:
        nb_algoritmoEntrenamiento = "train_gd"
    elif algoritmoEntrenamiento == 1:
        nb_algoritmoEntrenamiento = "train_gdm"
    elif algoritmoEntrenamiento == 2:
        nb_algoritmoEntrenamiento = "train_gda"
    elif algoritmoEntrenamiento == 3:
        nb_algoritmoEntrenamiento = "train_gdx"
    elif algoritmoEntrenamiento == 4:
        nb_algoritmoEntrenamiento = "train_rprop"
    elif algoritmoEntrenamiento == 5:
        nb_algoritmoEntrenamiento = "train_bfgs"
    elif algoritmoEntrenamiento == 6:
        nb_algoritmoEntrenamiento = "train_cg"
    elif algoritmoEntrenamiento == 7:
        nb_algoritmoEntrenamiento = "train_ncg"

    print ""
    print "Resultado final T + "+str(t)
    print ""
    print "Archivo: ", nb_archivo
    print ""
    print "Topologia: "
    print "NE: ", neuronasCentrada
    print "NH: ", neuronasCOculta
    print "FE: ", nb_algoritmoEntrenamiento
    print ""
    print "Unidades Estadisticas Normalizados"
    print "MSE: ", totalMSE
    print "RMSE: ", totalRMSE
    print "MAPE: ", totalMAPE
    print "SMAPE: ", totalSMAPE
    print ""
    print "Unidades Estadisticas Desnormalizados"
    print "MSE: ", totalDesMSE
    print "RMSE: ", totalDesRMSE
    print "MAPE: ", totalDesMAPE
    print "SMAPE: ", totalDesSMAPE
    print ""
    print ""


def MSE(originalList, forecastingList):

    sum = 0
    for index, elemet in enumerate(originalList):
        forecasting = forecastingList[index]
        sum = sum + ((forecasting - elemet)**2)

    return sum / len(originalList)

def Graficar(realValues, predictions):
    #Making the comparative graphic
    plt.subplot(211)
    plt.title("Forecasted Results vs Real Data")
    plt.xlabel("Time dimension")
    plt.ylabel("Value")
    plt.grid(True)
    plt.plot(np.asarray(realValues), 'b-', label='Real')
    plt.plot(np.asarray(predictions), 'r--', label='Forecast')
    plt.legend()
    #Making errors subplot
    plt.subplot(212)
    plt.ylabel("RMSE errors")
    plt.plot(np.asarray(rmseArray(predictions,realValues)), 'r-.')
    plt.grid(True)
    plt.show()

def rmseArray(predictions, targets):
    rmseRes = []
    pred = np.asarray(predictions)
    tar  = np.asarray(targets)
    for i in range(len(targets)):
        rmseRes.append(np.sqrt(((pred[i] - tar[i]) ** 2).mean()))
    return rmseRes

def guardarExcel(archivoCSV, tar, out, nb_columna1, nb_columna2):

    with open(archivoCSV, "wb") as ArchivoNuevoCSV:

        writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

        writer.writerow([str(nb_columna1), str(nb_columna2)])

        for i in range(0, len(tar)):
            tarTemp = str(tar[i])
            outTemp = str(out[i])
            aux1 = tarTemp.replace("[", "")
            tarAdd = aux1.replace("]", "")

            aux2 = outTemp.replace("[", "")
            outAdd = aux2.replace("]", "")

            writer.writerow( (tarAdd, outAdd) )

# Obtener datos sin normalizar
def funcionLeerArchivo2(archivo):
    valorIncorrecto = -1
    datosCrudos = np.genfromtxt(archivo , delimiter=',')

    return map(lambda x : "NA" if x == -1 else x, datosCrudos)

# Metodo para obtener distancia y numeo de huecos
def getNumHuecos(datos):

    arrayHuecos = []
    arrayDistancia = []

    tam = len(datos)

    x = 0
    distancia = 0
    banceraDis =  False
    existe = False
    inicio = True

    for i in range (0, tam):

        input = datos[i]
        if input == "NA":
            # print "i: ", i
            x += 1
            existe = True

            # if banceraDis and distancia != 0:
            if distancia != 0:
                arrayDistancia.append(distancia)
                distancia = 0

            # banceraDis =  True
        else:

            # if not inicio and banceraDis:
            
            distancia += 1

            if existe:
                arrayHuecos.append(x)
                x = 0 
                existe = False

        inicio = False

    # print "arrayDistancia: ", arrayDistancia

    return arrayHuecos, arrayDistancia

# Obtener datos sin normalizar
def funcionLeerArchivoParaImplantarHuecos(archivo):
    valorIncorrecto = -1
    datosCrudos = np.genfromtxt(archivo , delimiter=',')

    return map(lambda x : x, datosCrudos)