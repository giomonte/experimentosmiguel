import Helper
from random import randint
import numpy as np
np.set_printoptions(threshold=np.inf)


class Holes:
    def generate(self, originalSeries, distance, size):

        index = 0;
        lenght = len(originalSeries)

        seriesWithHoles = []

        while index <= lenght:

            # se genera un random desde 1 hasta la distancia sugerida
            randomDistance = randint(1, distance)

            newIndex = index + randomDistance

            originalSlice = originalSeries[index : newIndex]

            # se avanza en la serie la distancia obtenida y se mete a la serie con hoyos
            seriesWithHoles.extend(originalSlice)

            index = newIndex

            # si al agregar la distancia obtenida se llega al tamano de la lista original se termina el ciclo
            if index >= lenght:
                break


            # se obtiene un tamano de hoyo aleatorio entre 1 y el tamano de hoyo sugerido
            randomSize = randint(1, size)

            newIndex = index + randomSize

            # si la posicion actual de la serie con hoyos + los nuevos hoyos es mayor que la serie originalSerie
            # no se le agregan todos los hoyos obtenidos, solo los faltantes
            if newIndex > lenght:
                randomSize = newIndex - lenght

            for randomSizeIndex in range(randomSize):
                seriesWithHoles.append(-1)

            index = index + randomSize

        return seriesWithHoles

if __name__ == '__main__':
    rutaArchivo = 'datosVientoPruebas/wind_lapalma_3m_para_insertar_hoyos.csv'

    archivo = np.genfromtxt(rutaArchivo ,delimiter=',')

    holesDistance = 50
    holesSize = 10

    holes = Holes()
    seriesWithHoles = holes.generate(archivo, holesDistance, holesSize)

    print np.reshape(seriesWithHoles, (len(seriesWithHoles), 1))
